#!/usr/bin/env perl

use strict;
use warnings;
use Encode qw/decode_utf8 encode_utf8/;

sub markup {
  my $line = $_[0];
  my @line = split m{\|}, $line;

  my @base = split q{}, (shift @line);
  my @annotations = @line;

  if (scalar(@base) == scalar(@annotations)) {
    my $text = q{};
    while ( defined( my $word = shift @base ) ) {
      $text .= qq{${word}<rt>@{[ shift @annotations ]}</rt>};
    }

    return qq{<ruby>${text}</ruby>};
  } else {
    return qq{<ruby>@{[ join(q{}, @base) ]}<rt>@{[ join(q{}, @annotations) ]}</rt></ruby>};
  }
}

sub replace {
  my $line = decode_utf8(shift);
  
  $line =~ s<
      \{
        (
          [^\|]+
          (?: \| [^\|\}]+ )+
        )
      \}
  ><markup($1)>gex;

  return encode_utf8($line);
}

sub main {
  while ( defined( my $line = <STDIN> ) ) {
    print replace($line);
  }
}

sub testing {
  require Test::More;
  Test::More->import;

  is(
    replace('{約束されし勝利の剣|エクスカリバー}'),
    '<ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby>',
  );

  is(
    replace('{電子書籍|でん|し|しょ|せき}'),
    '<ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍<rt>せき</rt></ruby>'
  );

  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}

