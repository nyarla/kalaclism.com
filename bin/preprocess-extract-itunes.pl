#!/usr/bin/env perl

use strict;
use warnings;

sub extract {
  my $line    = shift;
  my ( $id )  = ( $line =~ m[
    \{\{\<
      \s*
        itunes
      \s*
        ([a-z]+[-]\d+)
      \s*
    \>\}\}
  ]x );

  return $id
}

sub main {
  while ( defined(my $line = <STDIN>) ) {
    if ( defined( my $id = extract($line) ) ) {
      print "${id}\n";
    }
  }
}

sub testing {
  require Test::More;
  Test::More->import;

  is(
    extract('{{< itunes jp:1035519927 >}}'),
    'jp:1035519927',
  );

  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}

