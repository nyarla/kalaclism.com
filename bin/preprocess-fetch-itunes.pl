#!/usr/bin/env perl

use strict;
use warnings;

use File::Spec;
use File::Basename;

use JSON::Syck;

use Furl;
use URI;

my $entrypoint = "https://itunes.apple.com/lookup";

sub fetch {
  my ( $country, $id ) = @_;
  
  my $uri = URI->new($entrypoint);
  $uri->query_form(
    id      => $id,
    country => $country,
  );

  my $ua = Furl->new(
    agent   => 'Fetch iTunes Information Agent/0.01',
    timeout => 10,
  );

  my $res = $ua->get($uri);

  if ( ! $res->is_success ) {
    return +{
      err     => !! 1,
      status  => $res->status_line,
      message => $res->content,
    };
  }

  my $data = JSON::Syck::Load($res->content);
  my $ret  = $data->{'results'}->[0];

  my $title     = $ret->{'trackName'}     // $ret->{'collectionName'};
  my $permalink = $ret->{'trackViewUrl'}  // $ret->{'collectionViewUrl'};
  my $image     = $ret->{'artworkUrl100'};

  return +{
    err       => !! 0,
    title     => $title,
    permalink => $permalink,
    country   => $country,
    id        => $id,
    image     => $image,
  };
}

sub process {
  my $path = shift;
  my $uid  = basename($path);
     $uid  =~ s{\.yaml$}{};

  my ( $country, $id ) = split qr{-}, $uid;
  my $data;
  my $size = -s $path;

  if ( ! defined($size) || (defined($size)) && $size <= 0 ) {
    $data = fetch($country, $id);
  } else {
    $data = JSON::Syck::LoadFile($path);

    if ( $data->{'expires_at'} <= time() ) {
      $data = fetch($country, $id);
    }
  }

  if ( $data->{'err'} && $data->{'state'} =~ m{403}) {
    return undef;
  }

  $data->{'expires_at'} = time() + ( 60 * 60 * 24 );
  return $data;
}

sub main {
  my $path = shift @ARGV;
  my $data = process($path);

  if ( defined($data) ) {
    JSON::Syck::DumpFile($path, $data);
  }
}

sub testing {
  require Test::More;
  Test::More->import;

  require File::Temp;
  File::Temp->import(qw/tempdir/);

  my $dir   = tempdir();
  my $path  = "${dir}/jp-1035519927.json";
  
  my $data  = process($path);

  is( ref($data), 'HASH' );
  ok( ! $data->{'err'} );

  diag( JSON::Syck::Dump($data) );
  
  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}


