#!/usr/bin/env perl

use strict;
use warnings;

sub extract {
  my $line = shift;
  my ( $asin ) = ( $line =~ m[
    \{\{\<
      \s*
        asin
      \s*
        ([a-zA-Z0-9]{10})
      \s*
    \>\}\}
  ]x );

  return $asin;
}

sub main {
  while (defined( my $line = <STDIN> )) {
    if ( defined( my $asin = extract($line) ) ) {
      print "${asin}\n";
    }
  }
}

sub testing {
  require Test::More;
  Test::More->import;

  is(
    extract('{{< asin XXXXXXXXXX >}}'),
    'XXXXXXXXXX',
  );

  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}

