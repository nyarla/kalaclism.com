#!/usr/bin/env perl

use strict;
use warnings;
use feature qw/state/;

use File::Spec;
use File::Basename;
use JSON::Syck;

use Furl;
use URI::Amazon::APA;

my $env_key       = uc 'preprocess_fetch_asin_config';
my $env_file_key  = uc "${env_key}_file";

sub config {
  state $config ||= do {
    my $file = $ENV{$env_file_key};
    my $data;

    if ( -e $file ) {
      $data = JSON::Syck::LoadFile($file);
    } else {
      $data = JSON::Syck::Load($ENV{$env_key});
    }

    $data;
  };

  return $config;
}

sub fetch {
  my $asin = shift;

  my $uri = URI::Amazon::APA->new(
    'http://webservices.amazon.co.jp/onca/xml'
  );

  $uri->query_form(
    Services      => 'AWSECommerceService',
    Operation     => 'ItemLookup',
    ResponseGroup => 'Medium',
    IdType        => 'ASIN',
    ItemId        => $asin,
    AssociateTag  => config->{'id'},
  );
  $uri->sign(
    key     => config->{'key'},
    secret  => config->{'secret'},
  );

  my $ua = Furl->new(
    agent   => 'Fetch ASIN Information Agent/0.01',
    timeout => 10,
  );

  my $res = $ua->get($uri);

  if ( ! $res->is_success ) {
    return +{
      err     => !! 1,
      status  => $res->status_line,
      message => $res->content,
    }
  }

  my $content = $res->content;

  my ( $permalink ) = ( $content =~ m{<DetailPageURL>\s*(.+?)\s*</DetailPageURL>} );
  my ( $title     ) = ( $content =~ m{<Title>\s*(.+?)\s*</Title>} );

  my $images        = [];

  for my $type (qw/ Swatch Small Tiny Medium Large /) {
    my ( $src     ) = ( $content =~ m{<${type}Image>(.+?)</${type}Image>} );
    my ( $url     ) = ( $src =~ m{<URL>\s*(.+?)\s*</URL>}                 );
    my ( $height  ) = ( $src =~ m{<Height[^>]+>\s*(.+?)\s*</Height>}      );
    my ( $width   ) = ( $src =~ m{<Width[^>]+>\s*(.+?)\s*</Width>}        );

    push @{ $images }, +{
      type    => lc($type),
      url     => $url,
      width   => $width,
      height  => $height,
    };
  }

  return +{
    err       => !! 0,
    title     => $title,
    permalink => $permalink,
    asin      => $asin,
    images    => $images,
  };
}

sub process {
  my $path = shift;
  my $asin = basename($path);
     $asin =~ s{\.json$}{};

  my $data;
  my $size = -s $path;

  if ( ! defined($size) || ( defined($size) && $size <= 0) ) {
    $data = fetch($asin);
  } else {
    $data = JSON::Syck::LoadFile($path);

    if ( defined($data->{'expires_at'}) && $data->{'expires_at'} <= time() ) {
      $data = fetch($asin);
    }
  }

  if ( $data->{'err'} && $data->{'status'} =~ m{403} ) {
    return undef;
  }

  $data->{'expires_at'} = time() + ( 60 * 60 * 24 );
  return $data; 
}

sub main {
  my $path = shift @ARGV;
  my $data = process($path);

  if ( defined($data) ) {
    JSON::Syck::DumpFile($path, $data);
  }
}

sub testing {
  require Test::More;
  Test::More->import;

  require File::Temp;
  File::Temp->import(qw/tempdir/);

  my $dir  = tempdir();
  my $path = "${dir}/B011J6GF1Q.json";

  my $data = process($path);

  is( ref($data), 'HASH' );
  ok( ! $data->{'err'} );

  diag( JSON::Syck::Dump($data) );

  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}

