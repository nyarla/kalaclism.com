+++
title = "このブログについて"
date  = "2016-01-21T16:53:49+09:00"
type  = "about"
+++

# カラクリズムについて

このブログは、

  * [岡村 直樹 (にゃるら)](http://nyarla.net/)

が、少なくとも 10 年は続ける、という目標を掲げた、作品、著作、あるいは記録としてのブログです。

そして今まで、他の自分のブログでは、デタラメなどは書かないように心掛けていたけれども、
運営に対しては手を抜いていた、という点を改め、面と向かってブログというモノを書いてみよう、
という目標を掲げた存在でも有ります。

まあ、どこまで人としての道が続くかは運次第ですが、出来うる限り永く、
このブログを続けたいと思っています。

# 著作権

このブログの著作権は、それが他者の著作でない限り、

  * [岡村 直樹 (にゃるら)](http://nyarla.net/)

が所持しています。

なお、ブログ内検索を実現するコードについては、

  * [MIT-licese](http://nyarla.mit-license.org/2016)

の基に利用することが出来ます。

# このブログを実現するための利用物

このブログでは、その機能を実現するために、下記のソフトウェア等を利用しています:

  * 自由なソフトウェア
    * [Hugo](http://gohugo.io/) - ブログのファイルの静的生成
    * [Perl](https://www.perl.org/) - ブログのファイル生成に関連するスクリプトで使用
    * [normalize.css](https://necolas.github.io/normalize.css/) - ブログのスタイリングの初期化 ([MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md))
    * [ampproject.org](https://github.com/ampproject/amphtml) - モバイル環境での閲覧の最適化 ([Apache 2.0](https://github.com/ampproject/amphtml/blob/master/LICENSE))
    * [jQuery](http://jquery.com/) ブログ内検索 ([MIT](https://github.com/jquery/jquery/blob/master/LICENSE.txt))
    * [lunr.js](http://lunrjs.com/) - ブログ内検索 ([MIT](https://github.com/olivernn/lunr.js/blob/master/LICENSE))
    * [lunr-languages](https://github.com/MihaiValentin/lunr-languages) - ブログ内検索 ([MPL](https://github.com/MihaiValentin/lunr-languages/blob/master/LICENSE))
    * [tinysegmenter.js](http://chasen.org/~taku/software/TinySegmenter/) - ブログ内検索 ([3-clause BSD](http://chasen.org/~taku/software/TinySegmenter/LICENCE.txt))
  * ホスティング
    * [AWS S3](https://aws.amazon.com/jp/s3/) - ブログ公開のためのホスティング
    * [Cloudflare](https://www.cloudflare.com/) - TLS 通信化と S3 へのアクセス数の低減
    * [ムームードメイン](https://muumuu-domain.com/) - ドメイン管理
    * [Github](https://github.com/) - ソースファイルの管理、及びホスティング

# 広告

このブログでは、このブログの収益化を目的として、

  * [Google Adsense](https://www.google.co.jp/intl/ja/adsense/start/)


に参加しています。

また、それぞれの広告ネットワークにおけるプライバシー等の訪問者情報の取り扱いの指針については、それぞれ、

  * [Google ポリシーと規約](https://www.google.co.jp/intl/ja/policies/privacy/)


を参照してください。

# 訪問者の追跡

このブログでは、訪問者のアクセス情報の解析、及び、このブログへのアクセス数の正確な把握のために、

  * [Google Analytics](https://www.google.co.jp/intl/ja/analytics/)

によるアクセス解析を行なっています。

そのため、このブログへの訪問者のアクセス情報については、
その利用サービスの特性上、上記利用サービス開発元の、Google 社と共有状態にあります。

また、今後、このブログを運営する中で、情報解析の委託等で、自分と訪問者以外の第三者する場合、
事前にブログのエントリとして告知した上で、アクセス情報を共有する場合が有り得ますので、その点、予めご了承下さい。

なお、収集した情報については、このブログの運営の参考にする以外に用ずることは無く、
それ以外の用途でも使用しない、と宣言致します。

# 連絡先

このブログについて、筆者である岡村 直樹に連絡を取りたい場合には、

  * Twitter: [@nyarla](https://twitter.com/nyarla)
  * Email: <nyarla@thotep.net>

まで、ご連絡下さい。

なお、 Email での連絡は、場合によっては当方 Email 環境で使用している、

  * [Google Apps](https://apps.google.com/intx/ja/)

のスパム判定に引っ掛かり、その連絡が未達になる場合が有り得ますので、
もし、Email 経由で連絡したのに、その後、音沙汰無い場合には、
Twitter 経由で、一度確認の連絡をお願い致します。

また、Email や Twitter 経由の連絡でも、その連絡が余りにも非常識で有る場合や、
あるいは、当方がその連絡に気がついていない、若しくは、その連絡に気がついていたとしても、
健康状態等の事情により、返信が行なえない場合も有り得ますので、その点はご了承下さい。

