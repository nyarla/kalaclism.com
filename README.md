[kalaclism.com](https://kalaclism.com/)
=============

  * A weblog for own thinking to everythings written by Japanese.

Copyright
---------

  * (c) 2016 Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

TODO
----

  * [x] Templates
     * [x] add custom `sitemap.xml` (?)
     * [x] add RSS or Atom Feed
     * [x] add google adsense IDs
     * [x] add google analytics id to configuration file
  * [x] Posts
     * [x] remove temporary amazon and itunes affilicate notice (these are not using yet)
  * [ ] WebSite
     * [ ] add favicon
  * [ ] System
     * [x] add full-text search mechanism for this blog
     * [ ] update notification for twitter or any others by notify as a service (Idea)
  * [x] Deployment
     * [x] automatic deployment by CI as a Service

